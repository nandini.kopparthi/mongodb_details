from flask import Flask
from scripts.service.mongodb_service import details

app = Flask(__name__)
app.register_blueprint(details)
if __name__ == "__main__":
    app.run()
