import pymongo


# Creating a client
def fun(var1):
    client = pymongo.MongoClient('localhost', 27017)
    # creating a database name GFG
    db = client['nandini']
    print("Database is created !!")
    coll = db["user_details"]
    coll.insert_many(var1)
