from flask import Blueprint, request

from scripts.handler.mongodb_handler import fun

details = Blueprint("color_finder_blueprint", __name__)


@details.route('/', methods=['POST'])
def post_json_handler():
    content = request.get_json()
    fun(content)
